# Fuga de la Isla Putrefacta
{text-align:center}

Un híbrido entre __Aventura D&D 5e__ y __Escape Room__, que permite a los aventureros compaginar una partida de rol calmada, al ritmo de la cárcel, y una interacción frenética, con la presión del crono, en el momento de la fuga:

 - Una aventura colorida llena de personajes memorables, abierta y llena de posibilidades, donde se plantean tanto combates razonables como mortales de necesidad
 - Un escape room lleno de enigmas, algunos originales y otros de fuentes poco habituales
 - Todo distribuido con licencias libres para que lo puedas adaptar como necesites :-)
 - Estamos trabajando en la segunda y tercera partes!

{{centered
_**De 3 a 6 aventureros de nivel 1**_
}}

{{note
Un grupo de jóvenes brillantes debe infiltrarse en la prisión más segura para encontrar al noble enano Florstin a tiempo de evitar una guerra.
}}


## Contexto

{{note
La guerra con los orcos de _Netherland_ ha terminado al fin. Tras la borrachera de la victoria, quedan atrás las historias de compañerismo y sacrificio, y nos golpea la realidad de la dura tarea de reconstruir el reino. Las heridas son profundas y desiguales. Las comarcas que llevaron la peor parte de la guerra ven ahora con recelo el rápido progreso de otras regiones que no contribuyeron a la victoria en la misma medida. Al frente del reino queda la joven reina __Alejandra__, que apenas conoció a su padre, y debe ahora ganarse el respeto de todos los habitantes del reino.
}}

Los más valientes y leales dieron su vida por el reino, y ahora la joven reina debe construir un nuevo reino por el que todos estén dispuestos a luchar, y para ello debe elegir sus ministros entre intrigantes cortesanos que supieron esquivar el peso de la contienda y que ahora parecen saberlo todo sobre cómo debe organizarse el reino. Sólo _su fiel **Florstin**_, que luchó valerosamente junto a su padre y su abuelo, merece la plena confianza de Alejandra. Sólo él tiene acceso a los códigos secretos, de entre todos aquellos mandarines que conocen el funcionamiento de los engranajes del reino... y ha desaparecido.

Justo antes de desaparecer, organizó las _Olimpiadas de la Juventud_, para seleccionar a jóvenes brillantes del reino y que se conviertan en personas de confianza de Alejandra. De hecho, aunque nadie lo sospecha, diseñó las pruebas finales de los juegos de modo que los personajes, que no sólo son brillantes, sino buena gente, resultasen ganadores.
Son estos jóvenes quienes formarán la compañía de los Compañeros de Alejandra, donde todos los reinos y razas del reino estarán representados... si sobreviven a su primera misión.

\column
## Comenzar

La aventura incluye personajes prediseñados. Puedes dejar que los personajes elijan de entre esa lista, o dejar que los jugadores creen personajes de nivel 1.

Si crean sus personajes, anímales a buscar personajes variados, con distintas clases, razas, trasfondos. Explícales que tiene sentido dentro de la aventura que el grupo sea diverso, pero todos deberían ser jóvenes. Además, los personajes provienen de regiones distintas, y por tanto no deberían ser hermanos ni amigos de la infancia, sino que todos se conocen en las primeras Olimpiadas de la Juventud. No debería haber ningún alineamiento malvado, y todos deberían moverse entre legal bueno, caótico bueno o legal neutral. Han de entender que en esta partida deben colaborar, incluso sacrificarse por el bien del grupo y del reino, y no mirar por su interés personal.
Por último, suma 1 a su mejor puntuación de característica y resta 1 a la peor, para representar que no sólo son héroes de nivel 1, sino los campeones del reino, y haz que cada cual elija el deporte en el que ha destacado, cuanto más estrambótico mejor.

No dejes que pierdan tiempo eligiendo el equipo. La primera escena es sin armas y con ropa de calle. Lo cierto es que en esta aventura no llegarán a usar el equipo inicial.
Como única excepción, permíteles usar, en el despacho del Duque, un canalizador o un arma relacionada con su deporte.

Los personajes prediseñados también pueden servir para que un jugador cuyo personaje muera pueda reengancharse rápidamente. Si alguno muere, aprovecha la ocasión para completar las capacidades de un equipo que ha quedado descompensado.
Es fácil reengancharlos: simplemente aparecen en el siguiente barco enviados de nuevo por Alejandra, transmiten la urgencia de la misión y se ofrecen a ayudar.
Son también campeones de otras disciplinas... pero si la muerte se produce cerca del final, déjales simplemente que controlen a Leonora y Florstin.

### Experiencia

Reparte `px` por resolver puzzles, tal y como se indica al final de cada puzzle, y también por resolver combates y por hitos, como entablar relación con otros presos, encontrar la guardia Grick o la biblioteca... 

Los `px` se pueden repatir también de forma heterogénea, a tu elección, si algún PJ participa de forma más decisiva que otros.

O puedes simplemente permitirles subir a nivel 2 después de que se hayan fogueado un poco en la prisión, por ejemplo cuando consiguen resolver unos pocos puzzles y crear su plan de fuga o cuando resuelvan un conflicto con la banda de Marna.

Cuando se ponga en marcha el plan de fuga todos deberían ser de nivel 2 o 3.

\page

### Metareglas / Seguridad

Te recomendamos elegir alguna seña, por ejemplo cruzar las manos, para que un jugador pueda indicar, sin cortar la narración, que no se siente a gusto con el rumbo que está tomando la secuencia, por exceso de violencia, contenido inapropiado, o el motivo que sea.

### Música (opcional)

Te _recomendamos_ algunos fragmentos de música para momentos concretos de la aventura, y otros que puedes usar de ambiente si quieres:

 - Para entrar en ambiente BSO Conan el Bárbaro - Column of Sadness - Theology
 - Para que se espabilen si están tardando al crear personajes o resolver un puzzle: [24 and 13 - the lunatic](https://seventythreerecords.com/24-and-13.html)
 - El mayordomo les conduce por palacio: Jean Baptiste Lully - Marcha para la ceremonia de los Turcos
 - Cuando se liberan las serpientes voladoras: Rimsky Korsakov - El Vuelo Del Moscardon
 - Cuando se embarcan: Rimsky-Korsakov - Scheherazade Op. 35-The Sea And Simbad's Ship
 - Remando: BSO Conan el Bárbaro - Column of Sadness - Wheel of Pain
 - Hablando con los presos en la prisión: Beethoven, Fidelio, Chorus of Prisoners
 - Confabulando: BSO The Great Escape - Main Theme
 - Trabajando en la mina : Verdi - Il Trovatore - Coro de los Gitanos
 - Persiguiendo o peleando con los grick: [Battle for Wesnoth: Knalgan theme](https://github.com/wesnoth/wesnoth/tree/master/data/core/music)
 - Puzzle de los hongos de Momo : [Marco Padiya](https://marcopadiya.bandcamp.com/album/despersonalizaciones) - Despersonalizaciones - 01 Alucinógeno
 - Llegan a la cripta : [Battle for Wesnoth](https://github.com/wesnoth/wesnoth/tree/master/data/core/music) - revelation y/o suspense
 - Si algún personaje muere, o al entrar al panteón: BSO Conan el Bárbaro - The Search
 - Cruzan la línea de las 3 hermanas y se liberan las Banshee : Mozart Dies Irae
 - Salen de la cueva: Grieg - Peer Gynt - 1. Morning Mood
 - Si hay batalla final en el barco - BSO Conan el Bárbaro - Riders of Doom y/o Battle of Mounds
 
\column

## Introducción

Al leer el texto siguiente, sustituye _X_ por el número de jugadores.

{{note
Las primeras __Olimpiadas de la Juventud__ del reino han sido un gran éxito. Talento, deportividad y entrega han estado presentes en todo momento. Impresionantes despliegues de magia, dificilísimas acrobacias y combates que han dejado como vencedores, en las distintas categorías, a _X_ jóvenes promesas del reino.
Compartiendo además comida y alojamiento durante los días de las pruebas finales, habéis desarrollado mutuo respeto y amistad.
La competición ha sido dura, y sois los mejores.

Y como colofón, tras la ceremonia oficial, con el Gran Estadio completamente lleno, os comunican que _la reina __Alejandra__ os recibirá en persona, y en privado_, para daros la enhorabuena.
Bien aseados y con elegantes ropas nuevas que parecen haber cortado a vuestra medida durante la ceremonia, recogéis vuestros enseres del albergue que ha sido vuestro hogar durante la semana de los juegos y salís hacia palacio.
No os acompaña ninguna comitiva oficial, y la invitación ha sido bastante discreta.
De camino, charlando, no podéis evitar pensar que en realidad es extraño que os reciba en privado, en vez de una fiesta llena de dignatarios.
}}

Entrega ahora a cada jugador uno de los rumores del apéndice I y deja que charlen un poco. Deja que cuenten su clase, trasfondo y raza, interviniendo si quieres para poner un poco de orden. Deja que inventen historias sobre su región, y que flipen si quieren. Al fin y al cabo, son adolescentes que acaban de ganar ;-)

Si preguntan por su premio, diles que tienen un trofeo que pueden canjear por dinero (unas 500po), pero lo tendrán _"más adelante"_.

## En el Palacio Real

El mayordomo real tiene instrucciones de llevar a los jugadores hasta un despacho poco transitado en palacio, evitando caminos concurridos y sin responder a nadie. Se le da especialmente bien hacerse el loco, usando la pompa de forma estratégica para que nadie haga preguntas.

![mayordomo](https://framagit.org/pang/escape_dd5e/-/raw/main/im%C3%A1genes/mayordomo.png) {width:200px}

\page

{{note,margin-top:30px
Un pomposo chambelán de edad avanzada os espera en una puerta lateral del palacio. _"Acompáñenme, por favor"_, y os guía subiendo escaleras y cruzando largos pasillos, saludando con vistosas reverencias a la poca gente con la que se cruza, pero sin dar explicaciones a la gente que mira con gesto de extrañeza. Cuando se detiene frente a una puerta doble, os da la sensación de que el camino ha sido innecesariamente largo.

Abre la puerta, y os invita a entrar. La habitación no parece ni el dormitorio de la reina ni una sala de recepciones oficial, sino un despacho con estanterías llenas de libros y pergaminos.
Dentro, la reina está leyendo pergaminos y al veros llegar sonríe, se levanta, os saluda y os indica con un gesto: _"Sentáos, por favor"_.

Es una muchacha humana de unos 16 años, pero transmite una serenidad y madurez que os sorprenden en alguien tan joven:

{{dialog

Espero que me disculpéis, porque iré directo al grano. Mi querido Florstin lo averiguó todo sobre vosotros. Es por eso que sé que puedo confiar en vosotros, y la realidad es que _necesito_ confiar en vosotros.

Florstin desapareció hace ya 3 decanas. Creo saber quién le ha hecho desaparecer, y no le interesa matarle, porque supondría la guerra con los enanos más allá de Minas Frías, pero pretende mantenerle apartado para el siguiente _Gran Concejo_.

He enviado personas de mi confianza a buscarle, pero no han podido dar con él.
Quiero que investiguéis los aposentos de una persona de quien sospecho, en busca de pistas sobre Florstin. Aprovechad ahora que ha salido fuera. ¡Tenéis poco tiempo!
}}
}}




### Los aposentos del duque de Albión
{margin-bottom:20px;}

{{note
El chambelán, disimulando sus nervios con la dosis habitual de pompa y cirscuntancia, sin decir una palabra, y mirando de reojo antes de girar en cada esquina, os conduce al despacho del duque. 

Las paredes están cubiertas de estanterías rellenas de registros y pergaminos, algunos en mal estado.
Algunos cajones del escritorio están abiertos, pero no tienen nada interesante. Otros están cerrados. También hay un cofre cerrado con llave.
Sobre el escritorio encontráis el diario del duque.
}}

![Despacho del duque de Albión](https://framagit.org/pang/escape_dd5e/-/raw/main/im%C3%A1genes/despacho.png)  {width:240px;margin-left:33px;margin-bottom:10px}

Si abren el cofre, saldrán volando tantas serpientes voladoras como aventureros.
El duque usa las serpientes para enviar mensajes a sus socios, y al abrir el cofre intentarán huir abriendo la ventana.
Si los aventureros se adelantan y mantienen cerrada la ventana, las acabarán cazando eventualmente, pero el chambelán les regañará si hacen ruido, y puede que algún aventurero se lleve algún mordisco...

Un conjuro de dormir terminará el combate inmediatamente.

{{monster,frame
___
> ## Serpiente voladora
> *Bestia Diminuta, no alineada*
>
> La serpiente voladora es una serpiente de brillantes colores y con alas que se encuentra en las junglas remotas. Las gentes tribales y los sectarios a veces las domestican como mensajeros que entregan pergaminos enrollados en sus colas.
> ___
> - **Clase de Armadura**: 14
> - **Puntos de golpe**: 5 (2d4)
> - **Velocidad** 30 pies, 60 pies volando, 30 pies nadando
>___
>|FUE|DES|CON|INT|SAB|CAR
>|:---:|:---:|:---:|:---:|:---:|:---:|
>|4 (-3)|18 (+4)|11 (+0)|2 (-4)|12 (+1)|5 (-3)|
>___
> - **Sentidos** Vista ciega 10 pies, Percepción pasiva 11
> - **Idiomas** ---
> - **Desafío** 1/8 (25 px)
>___
> - **Vuelo de reconocimiento**: La serpiente no provoca ataques de oportunidad cuando se aleja volando del alcance de un enemigo.
> ### Acciones
> ***Mordisco*** : Mordisco. *Ataque de arma cuerpo a cuerpo*: +6 al ataque, alcance 5 pies, un objetivo. *Impacto*: 1 punto de daño perforante más 7 (3d4) puntos de daño por veneno.

}}

\page

Si abren silenciosamente (con herramientas de ladrón CD15) los cajones, encontrarán 100po, 5 zafiros de 100po y un anillo de protección... que no les servirán de nada a menos que se los traguen o usen como supositorios ;-)

##### Puzzle: El diario del Duque

Cuando decidan inspeccionar el diario del duque, pon el crono en marcha.
Si buscan en torno a la fecha de desaparición del duque, entrégales la página titulada "Luis Cortés, III duque de Albión".
Hazlo también si buscan mucho tiempo.

#### Pistas

Si se retrasan, y cuando juzgues oportuno:

 - Tenéis que probar otro punto de vista
 - Tenéis que probar otro punto de vista... _literalmente_
 - Hay dos mensajes

#### Éxito: 100 px/personaje

Si encuentran el mensaje oculto mirando la página de diario de canto desde dos direcciones perpendiculares, y leen los registros, sabrán que Florstin está en la Isla Putrefacta.
Cuando salgan al pasillo, el chambelán les llevará ante Alejandra:

{{note
Alejandra escucha con interés vuestro descubrimiento:

{{dialog
Así que está en la Isla Putrefacta... sólo hay una manera de gestionar ésto: Tomad estos rubíes y guardadlos en vuestros bolsillos, ¡rápido!. ¡Confiad en mí! No os preocupéis, cuando lleguéis sabréis lo que tendréis que hacer.

¡Chambelán! ¡Guardias!
}}

El chambelán abre la puerta inmediatemente, acompañado por varios guardias.

{{dialog
¡Detenedlos! Los he descubierto robando mientras no miraba... (y guiñandóos un ojo): _¡Enviadlos a la Isla Putrefacta!_
}}
}}

## La Isla Putrefacta

Los personajes pierden todas sus posesiones: ni armas ni armaduras ni componentes ni canalizadores arcanos...

\column

{{note
#### El largo viaje
Tras unos cuantos días esperando en el calabozo, os encandenan dentro de un carromato con un pequeño ventanuco y os llevan al puerto de Ostok, a dos horas de viaje, donde os trasladan a la bodega de la galera que os llevará a la isla.

Hacéis el viaje encadenados en los bancos de remo de la cubierta inferior, junto a otros presos, acompañados por el olor del mar, las pisadas en el techo, el vaivén del barco en las olas, y el murmullo ininteligible de la comunicación que los marineros intercambian para manejar el barco, en el dialecto típico del este.

Para la mayoría de vosotros, es vuestro primer viaje por mar.
}}

Los personajes deben remar para hacer avanzar el barco, tirada de Atletismo CD10. Cada fallo se debe compensar con un nuevo intento, o por el compañero de remo. Para hacer un nuevo intento hay que hacer una tirada de Constitución CD 10, y aumenta en 5 con cada nuevo intento.
Un fallo en las tiradas de Constitución supone un nivel de cansancio (y 1D4 de PG por los latigazos). Los compañeros PNJ intentarán compensar un fallo de los jugadores si no han fallado ellos, pero se mostrarán menos dispuestos a charlar después. Al contrario, si un personaje hace no sólo su tirada, sino la de su compañero de remo, el compañero se mostrará después más amistoso.
Si un personaje alcanza tres niveles de cansancio será indultado de remar, pero también será insultado, y no le darán de comer, así que llegará a la isla con los tres niveles de cansancio, a menos que alguien le de su comida (sin que lo vea el guardia).

En la bodega hay varios presos encadenados junto a los personajes. 
Si el guardia les escucha hablar mientras reman les dará latigazos hasta hacerles callar, acompañando los latigazos con insultos _"¡Callad, escoria inmunda!"_, _"Vais a aprender a callar y obedecer aunque tenga que sacaros la piel a latigazos"_, _"Desayuno de orco"_, _"Cerebro de ogro, ¿no me has entendido?"_.

En los descansos, mientras comen, o por la noche, es posible hablar bajito con un compañero sentado justo al lado. Para hablar sin ser detectado, se puede elegir un momento en que el guardia esté lejos, o distraído.
Después de las primeras rondas de latigazos muchos no querrán hablar y será necesaria una prueba de persuasión para que hablen. La CD será 10, con los siguientes modificadores:

 - +5 si ese PNJ ha recibido latigazos
 - -5 si hay especial afinidad entre el personaje y su compañero de banco (la misma clase, raza o trasfondo)
 - +5 si hay especial animadversión entre el personaje y su compañero de banco (elfo y enano, erudito y salvaje, etc)
 - +5 si no reman, -5 si reman de más
 - -5 si comparten parte de la comida, o usan un conjuro de curación, o el truco guía, o tienen cualquier otro "detalle".
 - El modificar que consideres apropiado según cómo lo roleen.

![Rotten Island](https://framagit.org/pang/escape_dd5e/-/raw/main/im%C3%A1genes/isla2.png) {position:absolute,bottom:70px,left:-30px,width:450px}

{{artist,bottom:240px,left:50px
##### Rotten Island
[por Craiyon](https://www.craiyon.com/)
}}

\page
Cada personaje tiene un PNJ a su lado, y podrá intentar conversar (2 oportunidades) durante el viaje, mismas parejas.
Será necesaria una tirada de persuasión para conocer algo sobre el compañero de remo.
 - __Url__, bárbaro enano, minero de _Minas Frías_. Atle +5
 - __Yaveis__, semielfa de la baja nobleza de los _Alpes Negros_. Hermosa, orgullosa. Pone mucho interés en remar, aunque le cuesta. Atle +0
 - __Sunda__, mediana clérigo de curación de _Llanuras del Fulgor Violeta_. Amable, le cuesta seguir el ritmo remando. Atle -1
 - __Yak__, guerrero veterano de las NetherWars. Musculoso, rudo y de pocas palabras.  Atle +5
 - __Sofi__, humana pícara buscavidas de la capital. Atle +1

Si superan la tirada de Persuasión, dale en secreto el recorte correspondiente del apéndice I, y quítaselo al bajar del barco.
No podrá comentarlos con los compañeros hasta más adelante.

Reparte también entre `50px` y `100px` al bajar del barco, según hallan remado y dialogado.

### Llegando a la isla

![Rotten Island Map](https://framagit.org/pang/escape_dd5e/-/raw/main/im%C3%A1genes/rotten_island_map.png) {bottom:35px,margin-left:23px,width:260px}


{{note
El olor cambia poco a poco, recordándoos el nombre de vuestro destino. El ritmo de remada suave, las órdenes complejas y el ajetreo en el barco anuncian la llegada a puerto.

Salís de la bodega cargados de cadenas para ver frente a vosotros una isla montañosa, y comenzáis a caminar por un camino que avanza entre las montañas.
Al poco tiempo alcanzáis a atisbar una torre, después dos, hasta alcanzar la muralla exterior, con su doble puerta fortificada llena de guardias de aspecto rudo.

La construcción no parece obra del hombre.
}}

## La prisión  

La prisión tiene las áreas siguientes (__mapa A__):

 __A__ :: Comedor (de presos y guardias en turnos separados).
 __B__ :: Barracas de los soldados, los mineros profesionales, y el personal.
 __C__:: Patio. Delimitado por vallas claramente improvisadas mucho tiempo después de la construcción del complejo. Un único árbol crece en el patio, y su sombra es muy cotizada en los días calurosos. 
 __D__:: Dependencias de Pizarro
 __E__:: Biblioteca
 __F__:: Prisiones
 __G__:: Zona de aislamiento
 __H__:: Entrada a la excavación, cerrada por una verja
 __I__:: Puerta de entrada, fuertemente guardada en todo momento.
 __Muralla__ :: En todo momento hay guardias patrullando, se acercarán si ven algo sospechoso y darán la alarma si lo consideran necesario, pero no abandonarán sus puestos. Podrían lanzar lanzas o disparar con arco contra los presos en un intento de motín.

{{note
#### Bienvenidos a la prisión
Los nuevos presos pasáis uno por uno por la zona de aislamiento, donde abandonáis los uniformes grises y los grilletes del barco para despiojaros y cortar el pelo a quien lo tiene largo, sólo para poneros inmediatamente después los uniformes marrones y los grilletes de la prisión.

Ya no tenéis cadenas en los pies, pero lleváis pesados grilletes en las manos y el cuello, con argollas por las que se puede hacer pasar cadenas cuando es necesario. Como ahora mismo, por ejemplo...

Al salir, van enhebrando las argollas de vuestros cuellos para formar filas de 12 presos y os obligan a manteneros erguidos para el recibimiento de Pizarro, que os mira altivo y os emplaza:

_"En esta prisión tendréis una oportunidad de trabajar duro, y si lo hacéis, podréis enviar dinero a vuestras familias, o hacer que vuestro tiempo aquí sea más llevadero. También tendréis la oportunidad de entorpecer, vaguear, o intentar escapar... ¡pero sólo tendréis una!"_
}}

### Rutina

 - Los presos son encerrados por la noche en sus prisiones (__F__) y deben mantener silencio hasta que son despertados por una comitiva de guardias que avanzan tocando un gong.
 - Los presos son encadenados unos a otros por los grilletes y abandonan las prisiones para su inspección y limpieza durante la mañana. Se les lleva al patio (__C__), inusualmente amplio y luminoso para una prisión, con una valla precaria que demuestra que el complejo no fue construido para su uso actual.
    + Los guardias (perfiles de PNJ de guardia, veterano y caballero) vigilan a los presos desde detrás de la valla, y entran en grupos de 6 si ven algo sospechoso o hay una pelea, pero no antes de que vengan refuerzos a ocupar sus puestos en la valla, de modo que da tiempo a que las peleas terminen con varios presos inconscientes.
   
\page

<ul>

 - En caso de pelea, los guardias aislan a los implicados, los sacan a rastras sin entrar en detalles como quién inició la pelea, y después hacen formar para que Pizarro interrogue para averiguar quién más estaba implicado. Si se ven en esta tesitura, más vale que ayuden a resolver la pelea rápidamente, porque si Pizarro no queda satisfecho, usará los conjuros de _Detectar Pensamientos_ o _Zona de la Verdad_.
 
 - Si algún jugador usa la magia y lo ven los guardias, o si deja marcas a otro preso u objeto, le enviarán a la zona de confinamiento, la primera vez solo un día, pero más tiempo si reincide, y si usó la magia le marcarán con un pentáculo de tinta difícil de borrar.
</ul>

 - Una vez comprobadas las prisiones, un par de presos de edad avanzada salen del patio, les dan instrumentos de limpieza, y se ponen a barrer los patios.
 - Al poco, se piden voluntarios para ir a la excavación:
   + Los voluntarios deben hacer una fila, y son nuevamente encadenados y conducidos a la entrada de la mina (__H__).
   + El resto pasa el día en el patio, llueva o haga sol.
   + Más detalles sobre la excavación después...

 - Poco a poco van saliendo los presos de la excavación, se les cachea, se anota si su carretilla está llena con el tipo adecuado de grava, y se les lleva al patio.
   + En algunos momentos (los que más te convenga para que su plan funcione, o no...), solo hay guardias vigilando (ni caballeros ni veteranos).
   + Antes de la hora de comer, se hace sonar el silbato, y los presos que no hayan salido se dan por muertos. Entran los mineros profesionales a verificar y entibar, acompañados por guardias veteranos.
   + Se entrega una moneda (acuñadas en la misma prisión, no de curso legal) a los presos que hayan excavado hasta llenar su carreta (ver apartado _"La excavación"_). Si un preso no ha llenado la carretilla, al día siguiente no le darán comida por bajar a la mina.

 - Es importante que vayan al patio para que puedan hablar con Fiddle y que les ayude y
   oriente.

 - Se vuelve a encadenar a los presos para llevarlos al comedor. El rancho es nefasto para quien no entrega una moneda, y permite mantenerse pero no recuperar puntos de golpe o niveles de cansancio. Sí es posible recuperar cansancio o puntos de golpe con la comida que viene en la carretilla y el rancho.

 - Después de comer, los presos tienen un rato en las prisiones con las puertas abiertas. Es el momento para ducharse, jugar a los dados o charlar con otros presos discretamente (también para caer en las emboscadas de camorristas). Pero cuando suena el cuerno los presos, aún los más duros, se dirigen rápidamente a sus celdas, antes de que los guardias entren a toda velocidad golpeando con porras a todos los que encuentren en las zonas comunes. Si todos los presos están en su sitio, cierran las puertas.

 - Por la noche, otorga los `px` correspondientes. Aprox `30px` por entablar contacto con un preso reacio y `50px` por su primer día en la mina llenando la carretilla.
 
##### Puzzle: Monty Hall
Si se interesan por los juegos de dados, les puedes plantear la paradoja de Monty Hall, aunque no podrán apostar antes de haber ganado alguna moneda por otros medios:
 - Paga una moneda si pierde, gana una moneda si acierta. Cada jugador puede jugar una vez al día, y puedes conectar el crono hasta que encuentren la estrategia óptima o renuncien.

{{note
La apuesta es así: 
 - El trilero coloca una piedra dentro de uno de tres cubiletes. El jugador debe elegir un cubilete, y seguirle la pista es muy difícil.
 - Después de hacer su apuesta, el trilero levanta un cubilete vacío, y pica al jugador para que cambie su apuesta si quiere...
}}
Si insisten en seguir la pista a la piedra, haz que tiren percepción CD 20, pero _en un cubilete oculto_. Si fallan por menos de 10, sabrán que la han perdido y si fallan por más de 10, tendrán seguridad de haber acertado, aunque no sea así.

__Solución__ (`60px`): Interesa cambiar la apuesta siempre [wikipedia](https://en.wikipedia.org/wiki/Monty_Hall_problem).

Ante un gran éxito de los jugadores, o si tienen dinero suficiente, cierra el chiringuito sine die, ya que insistir no aporta...

La experiencia dice que _es muy raro que quien no conozca la apuesta encuentre la estrategia óptima_.
Omite esta parte por completo si no te interesa discutir la solución.

\page

### Vuestro hombre en la Isla 

__Fiddle Ironwill__ es un anciano enano que ha conseguido un trabajo limpiando, y no está dispuesto a perderlo. Parece distraído pero siempre pega el oído y se entera de todo.

Los jugadores no deben saber que Fiddle es en realidad Leonora, la mujer de Florstin, que entró en la prisión disfrazada de hombre. Conoce la existencia de la excavación, pero no cómo acceder. Si los jugadores empiezan a indagar, se dará cuenta, y se acercará a ellos desde el otro lado de la valla, haciéndoles muecas sin dejar de barrer (_"No me saludes, no me hables, no me mires, disimula: ¿qué estáis buscando?..."_). Ayudará a los jugadores si le prometen que él y un amigo suyo escapará con ellos. Si preguntan quién: _"ya lo sabrás..."_. Si preguntan por Florstin no podrá evitar responder: _"¿está aquí? ¿dónde?"_, pero rápidamente volverá a disimular: "Claro que se quién es... todo el mundo le conoce".

Si los jugadores se atascan, no dudes en hacer aparecer a Fiddle para darles consejos.
Puedes incluso hacer que sea otro potencial aliado, como Udar o Mike, quien les traiga el recado de que Fiddle quiere hablar con ellos.

### Otros presos

Si los jugadores obtienen buenas tiradas de percepción o perspicacia CD15, observarán que ciertos presos son _"interesantes"_ (alterna abiertos, y reacios, que requieren una tirada de persuasión para que aporten información interesante), mientras que si obtienen tiradas muy malas, sentirán que es buena idea entablar contacto con alguno de los presos _conflictivos_. La tirada debería ser secreta...


#### Presos interesantes y abiertos

 - __Mike__ es un joven humano que simpatiza fácilmente con los jugadores, incluso se acerca. No es muy informativo, pero les puede dar pistas sobre con quién hablar o los trucos de la prisión. Les dice que no bajen a la mina. Allí les atacaron unos monstruos con tentáculos, que se llevaron a una amiga a la que apreciaba mucho. No pudo hacer nada por evitarlo, y no quiere volver. Dice que es por esos monstruos por los que envían a los presos a excavar solos, y luego los entibadores bajan con fuerte escolta.

 - __Momo__ es una bruja que ha pasado mucho tiempo en aislamiento por usar la magia y parece que ha perdido la cordura. Pasa el tiempo interactuando con lagartos e insectos. Hace años bajó a la mina, y si se entera de que los jugadores van a bajar, les pedirá que le consigan unas setas que permiten cocinar una poción que permite "viajar con la mente como si no hubiera paredes".

   Momo puede ser una buena compañera de celda. Parecerá que está loca y no tiene nada que
ofrecer hasta que llegue su momento. Otra forma de destacar a este personaje es que se
puede ofrecer a lanzar "buenas bayas" si le consiguen algo de muérdago. También podría
lanzar palabra de curación, que aunque no tenga un efecto real en el juego porque lo
haría justo antes de un descanso, dará la pista a los PJ de que Momo es más interesante de
lo que parece. También podría preparar una infusión para mejorar el descanso de los PJ,
sumándoles un bonificador de +3. Momo no pide nada a cambio de su ayuda, pero no debes
revelar demasiado pronto que puede ayudarles, sino que debe parecer una loca que se ríe
sola.

##### Puzzle: el viaje de Momo

Se necesita exactamente un tipo de seta: __Psilocybe pelliculera__. Pero en las cuevas hay más tipos. Las explicaciones de Momo con poco claras ("tiene un aura eléctrica, pero evita otras que tienen un aura picante..."), y tendrán que buscar un libro en la biblioteca, tras una tirada de Investigación CD10, que se presenta como Apéndice II.

Después de conseguir el libro, harán una tirada de Naturaleza CD10 para encontrar las setas.
Sin el libro de la biblioteca, y sólo con las indicaciones de Momo, es una tirada de Naturaleza CD18.

 - Si fallan por menos de 5, habrán conseguido una buena cena, por haber consumido _Cantharellus Sicarius_.
 - Si fallan por más de 5 pero menos de 10, tendrán una buena resaca por consumir _Agaricus Astrodermus_ : dos niveles de cansancio.
 - Si fallan por más de 10, tendrán una enfermedad por consumir _Amanita Buscalio: pierde 3 a la sabiduría, y tres niveles de cansancio. La reducción se puede recuperar mediante un conjuro de restablecimiento mayor, que puede lanzar Fiddle, pulverizando uno de los diamantes de la cripta, cuando se hayan ganado su confianza.

Haz la tirada en privado hasta que encuentren la seta adecuada.
Si te interesa por lo que sea y fallan buscando setas, puedes hacer aparecer un _grick_, un _grell_ o un _mimeto_ si te resulta más divertido.

Si consiguen las setas adecuadas, y buscan un lugar tranquilo para mezclarlas, arranca el crono y entrégales el estereograma que contiene el número de celda de Florstin. 
Si fallaron buscando la Psilocybe, tacha las setas que ya hayan probado en cuanto lo pidan.

Si comen el brebaje en cualquier sitio y lugar que no sea la mina al principio de la jornada, o su celda de noche, pasarán varias horas "viajando por la prisión", y a menos que otros compañeros que no hayan comido el brebaje ayuden, el personaje se despertará en la celda de confinamiento.
Dale cuerda a esta cometa, con los jugadores intentando disimular o camelarse a los guardias si crees que puede ser divertido.

\page

__Solución:__ (`120px`) 17 => ubicación de Florstin. Si buscan una salida, pueden tirar percepción CD10 para averiguar que hay un túnel accesible desde la excavación que sale al mar, pero hay que bucear y no sabrán exactamente dónde (sí verán "tres hermanas elfas que dan bastante miedo"). Cada persona que tome la mezcla puede hacer una tirada.

_Extra_: Esta mezcla se puede consumir en cualquier otro momento. Sus efectos serán: 
 - desventaja en todas las tiradas de ataque y pruebas de característica
 - ganas o recuperas tantos espacios de conjuro como tu máximo, en cada nivel (ej: un personaje con 3 espacios de nivel 1 que ya ha gastado dos espacios obtendría 3 espacios más de nivel 1 hasta tener un total de 4)
 - al lanzar un truco o conjuro tienes ventaja en las tiradas de ataque y tus adversarios desventaja en las tiradas de salvación para resistirse a sus efectos.

#### Presos interesantes pero reacios

Los presos que venían en el barco con ellos son "interesantes", pero si no se ganaron su confianza en el barco, seguirán siendo reacios a hablar (persuasión CD10) y cooperar (persuasión CD15 a CD20) y habrá que ganarse su confianza compartiendo comida, trabajando con ellos, o salvándoles de los grick de la excavación...

 - +5 si en el pasado un intento de contacto con este PNJ fracasó
 - -5 si hay especial afinidad entre el personaje y su compañero de banco (la misma clase, raza o trasfondo)
 - +5 si hay especial animadversión entre el personaje y su compañero de banco (elfo y enano, erudito y salvaje, etc)
 - -5 si le ayudan a llenar la carretilla en la mina
 - -5 si comparten parte de la comida, o usan un conjuro de curación, o el truco guía, o tienen cualquier otro "detalle".

Más presos reacios a cooperar:

 - __Udar__ es una mediana aficionada a la música a la que pueden descubrir canturreando o haciendo diagramas en la arena. Si se acercan cuando dibuja, borrará los diagramas, y dirá que "no es nada" (y +5 al CD para establecer contacto). Pero si se acercan cuando canturrea y le preguntan por la canción, -5 al CD, y si los personajes superan Interpretación CD10 para cantar con ella, será automáticamente amistosa

   Si se ganan su confianza les contará que la bibliotecaria a veces paga a presos para que le ayuden a clasificar libros, o con sus investigaciones. En la biblioteca hay muchos libros interesantes, y el rato se pasa entretenido... pero la bibliotecaria está interesada en ciertos enigmas relacionados con los gigantes, y si no puedes ayudarle, durarás poco como ayudante.

 - __Potencial cómplice__: inventa personajes genéricos dispuestos a colaborar en un plan de fuga. Dales nombre, para que no sepan claramente cuáles con importantes y cuáles no... Si abusan, ignora el resultado del dado y ponles delante un camorrista o un chivato.
    + __Lindra__, marinera elfa: "no todos los guardias de la prisión son veteranos"
    + __Worin__, herrero enano: "por 3 monedas puedes comprar un colchón mejor: merece la pena"
    + __Sutra__, guerrera tribal humana: "la gente que va a las celdas de confinamiento sale muy perjudicada"

 - Estos potenciales cómplices pueden también dar consejos de veterano en una segunda interacción. Elige el que más te convenga:
    + No vayáis por ahí hablando con cualquiera. Hay chivatos en la prisión que le contarán cualquier plan de fuga a **Pizarro**.
    + Las minas estaban infestadas de monstruos, pero los guardias hicieron una batida hace poco y la cosa no está tan mal.
    + Los monstruos de la mina arrastran a sus víctimas hasta su guarida, que debe estar llena de tesoros.

#### Presos conflictivos

Cada fallo por más de 10 en la tirada de Perspicacia hará parecer interesante una conversación con uno de estos personajes. Alterna de uno y otro grupo. 
Son todos indiferentes, pero es bueno que leas su nombre y trasfondo para que no sospechen.

##### La banda del dragón blanco

Esta banda de matones está liderada por **Marna**, guerrera semiorca de Llanuras del Fulgor Violeta.
Si los PJs fallan una tirada de perspicacia, pueden sentir que es buena idea establecer
contacto con Marna o alguno de sus secuaces.

Por ejemplo, uno de sus secuaces le podría preguntar a un PJ qué está buscando e,
independientemente de la respuesta, decirle que tiene que conocer a Marna.

Intentarán llevar a uno o más jugadores a un entorno favorable con superioridad numérica,
para _hablar en confidencia_. Allí intentarán robarle o proponerle que se una a su banda,
y darle una paliza si no lo hace. Si el grupo es adulto, puedes jugar con situaciones más
tensas. Por ejemplo, Marna podría subir a Oda a su regazo y manejarla como una marioneta,
o llevar a Sven a las duchas.

\page

Marna tiene un perfil de *capitán bandido*, y el resto de la banda son meros *matones*,
pero sin armas (o con garrotes) y sin armadura (CA10). Para 3 PJ, se enfrentarán solos a
Marna, pero añade un matón más por cada PJ por encima de 3. El encuentro debería ser
mortal para PJ de nivel 1, y difícil para PJ de nivel 2, pero si pierden no morirán, sino
que se despertarán doloridos, humillados y despojados de todo lo que hayan podido acumular.
Además, el conflicto seguirá abierto, y Marna y los suyos intentarán intimidar a los PJ y 
abusar de ellos.

Los PJ pueden pedir ayuda a otros presos (como el clan del martillo) o resolver un conflicto
con Marna de cualquier otra forma.

Si el conflicto se desata, los jugadores deben tener cuidado de no mostrar su magia o
hacer demasiado ruido, o un jugador podría ir a la zona de aislamiento. Si los ataques
mágicos de un jugador dejan secuelas (descarga de fuego, rociada venenosa), deberán
intimidarles o sobornarles para que no hablen.

##### El clan del martillo

Banda de enanos y enanas liderada por **Saúl**, *veterano* de Minas Frías. No son una
banda de matones, sino un grupo que aporta a sus miembros apoyo y sentimientos de
pertenencia a costa de mostrar desdén y a menudo infundir miedo al resto de presos. Los
miembros del clan son especialmente reacios a colaborar, con un +10 a la dificultad de
interactuar y se mostrarán agresivos y abusones con cualquier PJ que intente una tirada de
persuasión y la falle.

Suelen moverse en compañía de otros miembros del clan, y bajan juntos a las minas, donde
encuentran la manera de terminar en una o dos galerías, siempre acompañados por otros
enanos del clan. Es buena idea que un PJ termine en una galería acompañado por enanos del
clan, para que pueda escuchar sus rimas.

Los enanos cantan rimas mientras trabajan en la mina, e inventarse rimas con ellos es una
manera de ganarse su favor.
Algunos ejemplos de rimas:
 - "A la mina no traigas tus anillos / dobla tu espinazo por un bocadillo"
 - "Si sientes que algo se mueve en las rocas / ¡Aléjate! Toda precaución es poca"
 - "Agarra tu pico, golpea sin parar / sal con tu carreta y no mires atrás"
 - "Codo con codo, mano a mano / picamos juntos todos los enanos"

Otra forma de ganarse su favor, a discreción del DJ, podría ser emborracharse con ellos en
las celdas, ofreciéndoles licor destilado casero que pueden conseguir con la ayuda de
algún otro preso, por ejemplo escamoteando arroz para luego fermentarlo.

Evitar un conflicto abierto con este grupo de enanos debería ser fácil, simplemente
ignorando sus provocaciones. Sin embargo, son ruidosos, orgullosos y pendencieros, harán
desplantes y provocarán a los PJ, y no dudarán en empezar una pelea a puñetazos. Sin
embargo, si los PJ usan cualquier tipo de arma o magia contra ellos, llamarán a sus
compañeros, usarán sus picos, y se desatará un conflicto difícil de cerrar.
Los enanos tienen un perfil de _guerrero tribal_ y Saúl de _caballero_, pero usan picos que hacen 1d4 de daño y tienen clase de armadura 10.

Si los jugadores tienen un éxito rotundo en el combate, pero son magnánimos (por ejemplo,
reducen los PG de Saúl y conminan al resto a rendirse), el clan del martillo también podría
convertirse en un aliado. Y en cualquier caso, lidiar adecuadamente con los camorristas hará
que el resto de presos se tomen a los jugadores más en serio (+5 a la tirada de persuasión
para hacer cómplices). _150px/PJ_ por el combate.

Mencionar a Florstin a los miembros de este grupo no traerá consecuencias negativas, sino
miradas preocupadas entre ellos y amenazas de que no deben ir por ahí mencionando su
nombre. Además, si mencionan a Florstin, Saúl en persona se acercará en el patio para
hablar con los PJs. Fiddle podría mediar entre los PJ y este grupo, y una vez se
conviertan en aliados, podrían ayudarles a crear una distracción o a deshacerse de Marna y
sus secuaces.

##### Chivatos

Mucho peores que los anteriores: si los jugadores revelan demasiada información, se
mostrarán muy interesados, se unirán a posibles planes de fuga, etc., pero en el peor
momento aparecerán varios soldados (ver _plan de fuga_) y el chivato se pasará a su lado.
Si un jugador sospecha, puede tirar Perspicacia CD12.

 - **Phil**, mediano marinero del puerto de Ostok. _"Haría cualquiera cosa por volver a ver a mi familia"_.
 - **Agatha**, humana acróbata de la capital. _"Para salir de esta fortaleza no hay que tener miedo a nada"_.

\page

### Magia en la prisión

Usar magia se considera una ofensa al orden equivalente a usar un arma. Si los guardias descubren que alguien es lanzador de conjuros, le marcarán con un pentáculo que tardaría meses en borrar frotando, estarán más atentos y lo llevarán esposado con más fruición de lo habitual.

Los jugadores no tienen canalizadores mágicos, así que no podrán lanzar conjuros que requieran componentes materiales.
Los magos además no tienen su libro de conjuros, pero hemos colocado un libro en la guarida grick para resolver este problema.

Se pueden conseguir los componentes para los siguientes conjuros interesantes para la aventura:
 - En lo alto del árbol del patio hay muérdago (componente necesario para el conjuro _"buenas bayas"_, ideal para hacer amigos ;-) ), y un nido de pájaro del que pueden obtener plumas para lanzar _"caída de pluma"_.
 - Un druida podría usar una rama de muérdago como canalizador mágico, pero es demasiado grande y no podría ocultarlo.
 - Las ramitas caídas del árbol se podrían afilar con un arma de filo para lanzar _"crecimiento espinoso"_.
 - Udar podría fabricar una flauta para un bardo, pero habría que darle madera, un cuchillo, y 4 horas... y pensar dónde esconderlo :-/
 - En el patio se puede capturar un grillo, necesario para lanzar _"dormir"_.
 - En la biblioteca hay goma arábiga, necesario para _"invisibilidad"_.
 - En la mina se usa _"vellón"_ para limpiar las herramientas al terminar. Sirve para lanzar _"armadura de mago"_, _"ilusión menor"_, _"imagen silenciosa"_. Tb se puede conseguir cobre para lanzar _disfrazarse_.
 - En la biblioteca hay pergamino para lanzar _escudo de fe_.
 - En la guarida grick hay diamantes, necesarios para que Fiddle pueda lanzar _"restablecimiento mayor"_.
 - Usad la imaginación si necesitan otra cosa :-/
 
\column
### La biblioteca

**Atena**, la bibliotecaria, está interesada en ciertos enigmas relacionados con los gigantes.
Si los jugadores los resuelven, aceptará a uno de ellos como colaborador y podrá acceder a información importante para resolver ciertos puzzles. Además, cada día de colaboración obtendrá tres monedas. Si un jugador pasa el día en la biblioteca, haz que la jornada en la mina del resto transcurra sin incidentes.

El primer contacto será un día, al formar para ir al patio, después de que Fiddle, Momo o Udar hayan mencionado a la bibliotecaria.

{{note
Mientras estáis encadenados, esperando para ir al patio, aparece una mujer elfa de unos quinientos años con un vestido blanco, moño, y unas gafas apoyadas en la nariz que parece que sólo necesita para ver de cerca.
Observa atentamente a los presos, especialmente a los nuevos, y aún más especialmente a los que parezcan eruditos, bardos, druidas, magos... más que guerreros o maleantes. Parece tener buen ojo...

{{dialog
Escuchadme atentamente: soy la bibliotecaria de este complejo.
Fui designada para estudiarla antes de que la mayoría de vosotros naciérais, antes incluso de que esta isla se convirtiera en una prisión.

Busco gente inteligente que me ayude a catalogar los nuevos volúmenes sobre historia de los gigantes.
Estos libros están llenos de enigmas endiablados, pensados para que sólo los gigantes de las tormentas, eternos y omniscientes, pudieran resolverlos.
Quien pueda resolver estos enigmas será mi colaborador, y recibirá tres monedas por ayudarme a catalogar los libros sobre gigantes.

¿Os interesa? Escuchad el primero:
}}
}}

##### Puzzle: la fiesta de los gigantes
{{note
A una fiesta de gigantes asistieron:
 - ___T___*runar*, gigante de la tormenta
 - ___N___*ivios*, gigante de las nubes
 - ___F___*lavia*, gigante del fuego
 - ___E___*lia*, gigante de escarcha
 - ___P___*etros*, gigante de piedra
 - ___C___*rog*, gigante de las colinas

Si T saludó a cinco gigantes, N a 4, F y E a 3, P a 2 y C a 1: ¿a quién saludó exactamente __E__? 
}}

__Solución__: (`60px`)  T saludó a todos, luego C saludó a T y a nadie más, luego N saludó a T, F, E y P, luego P saludó a T y a N y a nadie más, luego F saludó a T, N y E, y por último E saludó a T, N y F.
\page

#### Ajedrez de gigantes

{{note
El ajedrez actual para criaturas pequeñas deriva del auténtico juego de gigantes.

Los gigantes enviaban mensajes en forma de partidas de ajedrez, de los tiempos en que había dos reyes de los gigantes, con sus respectivas cortes:

 - *Rey*, gigante de la tormenta
 - *Reina*, gigante de las nubes
 - *Torres*, gigante del fuego
 - *Alfiles*, gigante de escarcha
 - *Caballos*, gigante de piedra
 - *Peones*, gigante de las colinas 

Se presenta un tablero de ajedrez con el juego empezado, y no se trata de decir cómo ganar, sino de averiguar qué ha pasado.
}}

Elige de entre los puzzles siguientes, en orden creciente de dificultad. 

##### Puzzle: El gigante de piedra

{{note
Partiendo de la posición normal de salida del ajedrez, quita un caballero blanco: ¿qué dos mensajes nos transmiten?
}}

![puzzle ajedrez](https://framagit.org/pang/escape_dd5e/-/raw/main/puzzles/ajedrez_falta_caballo.png) {width:250px;margin-left:20px;}


**Solución:** (`30px`)

 1. Un gigante de piedra blanco ha sido capturado
 2. Un gigante de piedra negro lo ha capturado

##### Puzzle: El gigante de las colinas

{{note
A los gigantes de las colinas les gusta imitar a otros gigantes de rango más alto. ¿Qué gigante de la corte es en realidad un gigante de las colinas disfrazado?
}}
![puzzle ajedrez](https://framagit.org/pang/escape_dd5e/-/raw/main/puzzles/ajedrez_donde_esta_el_peon.png){width:250px;margin-left:20px;}


**Solución:** (`60px`) El alfil blanco no ha podido salir de su posición, luego lo debió capturar un caballo negro, y el peón blanco de dama coronó como alfil blanco

##### Puzzle: ¿Cuál es la corte?
{{note
La ubicación de la corte blanca y negra es desconocida.
¿En qué lado comenzaron las blancas, en el de arriba o en el de abajo?
}}

![puzzle ajedrez](https://framagit.org/pang/escape_dd5e/-/raw/main/puzzles/ajedrez_que_lado_blancas.png) {width:250px;margin-left:20px;}

**Solución:**  (`60px`) El único posible movimiento que da lugar a esa configuración es con las blancas avanzando hacia abajo, y un peón coronando en el alfil de h1 mientras captura una pieza negra.

Hay muchos más puzzles de ajedrez en los libros de Raymond Smullyan: "Los misterios ajedrecísticos de Sherlock Holmes" y "Los caballeros de Arabia".
Úsalos si los anteriores resultan demasiado fáciles a tus jugadores.

\page
### Descanso

Cada fracaso en las tiradas de excavar en la mina supone un nivel de cansancio, así como quedarse a cero PG, aunque después te devuelvan PG con algún conjuro o como sea.

Para recuperar PG, espacios de conjuro, y un nivel de cansancio, es necesario superar una tirada de Constitución CD15, con modificadores:
 - +5 a la tirada si ha pagado la comida buena, o ha bajado a la mina y ha comido la comida de la mina
 - +5 a la tirada si tenía todos los PG
 - +5 a la tirada si el colchón es bueno

### Confinamiento

Un preso que altere el orden pasará un día en confinamiento (¡envíale a buscar bebidas y que le cuenten lo que se ha perdido luego ;-) !) y serán dos días si reincide (¡envíale a preparar un aperitivo!). Si reincide de nuevo, quedará en confinamiento hasta que comience el rescate final. Podrán liberarle a la vez que a Florstin.

En el confinamiento no se recuperan PG ni espacios de conjuro ni desciende el cansancio, pero además será necesaria una tirada de Constitución cada día para no perder un nivel de cansancio más. La dificultad comienza en CD 10, pero aumenta en uno cada día de confinamiento.
Si el PJ está en confinamiento por haber atacado a algún guardia, no le permitirán recuperar sus PG, y si no había perdido PG durante el combate, perderá la mitad en una paliza posterior.

### Economía

Con las monedas que consigan excavando, ayudando en la biblioteca, o trapicheando, podrán comprar mejor comida (1 pieza por la comida de un día), incluso un colchón más cómodo (5 monedas).

\column

## La excavación

La excavación avanza en paralelo en varias galerías. 
Para entrar, los presos deben presentarse voluntarios cuando se ofrece la posibilidad, poco después de salir al patio.
Los voluntarios forman una fila, y son nuevamente encadenados y conducidos a la entrada de la mina (__H__) donde se les entrega una carretilla con pico, pala, comida, agua, linterna y vellón para limpiar las herramientas, que deben volver limpias y en buen estado.

Se indica a los presos en qué galería debe trabajar. El criterio es sencillo: si hay _n_ jugadores, los primeros _n+1_ presos van a la galería 1, los siguientes _n+1_ a la galería 2, etc.
Los jugadores deben buscar la manera de deshacerse del intruso, o de meterle en el ajo.

Para llenar la carretilla es necesaria una tirada de atletismo CD14. Si se falla, se puede volver a intentar, pero cada reintento obliga a hacer una tirada de salvación de constitución CD10 o ganar un nivel de cansancio. Se puede intentar llenar dos carretillas o más, pero cada carretilla añade un +4 a la CD de la tirada de constitución y el máximo es de 3 al día. Se puede usar el truco guía, pero sólo lo puede usar un jugador.

Si un jugador vuelve con la carretilla vacía, no le darán dinero, ni al día siguiente pondrán comida en su carretilla.
Además, los guardias y/o los compañeros podrían burlarse, a discreción tuya.
Es posible, y muy sensato, sobornar a alguien para que saque otra carretilla. Pedirá una moneda, y solo lo intentará cuando termine la suya, y si no está cansado.
Eso sí, deben tener cuidado e interpretarlo bien, o levantarán sospechas entre los chivatos.

Si los PJ usan los picos de minero como arma, por ejemplo contra los grick, cualquier pifia rompe el arma, tendrán que dar explicaciones, y pagar una moneda por la herramienta.


#### Guarida grick

En las galerías 2 y 4 hay agujeros por donde entran grick y atacan a los mineros. Si los aventureros siguen uno de esos agujeros, tendrán un encuentro _difícil_ (según la tabla de umbrales) con varios grick (un grick por cada 3 jugadores de nivel 2).


![tabla umbrales experiencia](https://framagit.org/pang/escape_dd5e/-/raw/main/umbrales_PX_up_tp_5.png) {bottom:35px,margin-left:-3px,width:300px}

Mike, Fiddle, Worin, Sutra u otros presos que bajan a la mina saben que la galería 2 es problemáticas, e intentan colocarse en otras posiciones de la fila.
Se lo pueden decir a lo jugadores si lo preguntan, o también se pueden dar cuenta si son observadores.

\page

![Grick](https://framagit.org/pang/escape_dd5e/-/raw/main/im%C3%A1genes/grick_by_craiyon.png) {margin:10px,width:260px}

{{monster,frame
___
> ## Grick
>*Monstruosidad Mediana, neutral*
> ___
> - **Clase de Armadura** 14 (armadura natural)
> - **Puntos de golpe** 27 (6d8)
> - **Velocidad** 30 ft., climb 30 ft.
>___
>|FUE|DES|CON|INT|SAB|CAR
>|:---:|:---:|:---:|:---:|:---:|:---:|
>|14 (+2)|14 (+2)|11 (+0)|3 (-4)|14 (+2)|5 (-3)|
>___
> - **Resistencias al daño:** Contundente, perforante y cortante de ataques no mágicos
> - **Sentidos** Visión en la oscuridad 60 pies, Percepción pasiva 12
> - **Idiomas** ---
> - **Desafío** 2 (450 px)
>___
> ***Camuflaje en piedra*** : El grick tiene ventaja en las pruebas de Destreza (Sigilo) que haga para esconderse en terreno rocoso.
> 
> ### Acciones
> ***Multiataque*** : El grick realiza un ataque con sus tentáculos. Si impacta, puede hacer un ataque de pico contra el mismo objetivo.
> 
> ***Tentáculos*** : *Ataque de arma cuerpo a cuerpo*: +4 al ataque, alcance 5 pies, un objetivo. *Impacto*: 9 (2d6+2) puntos de daño cortante.
> 
> ***Pico*** : *Ataque de arma cuerpo a cuerpo*: +4 al ataque, alcance 5 pies, un objetivo. *Impacto*: 5 (1d6+2) puntos de daño perforante.
>
> Repugnante babosa gigante dotada de un pico de loro y cuatro tentáculos que acaban en ganchos. Esta abominación caza desde la sombras entre las rocas, esperando pacientemente a sus presas para emboscar con brutalidad.

}}


Los agujeros son difíciles de encontrar: puedes poner una tirada de percepción o supervivencia CD18 si lo buscan activamente, y dar ventaja a un explorador experto en cuevas.
Quizá es más interesante que un grick ataque a un aventurero o PNJ: si los aventureros le ponen en fuga, o si derrota fácilmente al PNJ, los aventureros pueden seguirle hasta la guarida. Allí encontrarán armas y el resto del tesoro, pero escucharán ruido de un grick acercándose por la otra galería y en cuanto se hayan pertrechado (1 minuto, da tiempo a ponerse armadura ligera) aparecerán más grick, y los aventureros podrán huir o pelear. Huir es fácil porque el túnel es estrecho y sólo cabe un monstruo. Conquistar la guarida es más interesante, porque tendrán todo el tesoro, y además un escondite donde dejarlo.

_Tesoro_: En la guarida de los grick encontrarán armas y armaduras de varios guardias (5x lanzas, 3x escudos, 5x espadas cortas, 3x espadas largas, 5x cotas de malla, 5x armaduras de cuero), herramientas de minero a mansalva, 300po, 30 monedas de la prisión, un diamante de 300po, 8 gemas verdes con forma de lágrima y un libro de conjuros gastado con _caída de pluma_, _dormir_, _imagen silenciosa_, _hechizar persona_, _disfrazarse_, _invisibilidad_, _detectar pensamientos_.
Añade también arcos cortos, un estoque o cualquier otro equipamiento básico que algún personaje tenga en su equipo básico.

Además, hay varios uniformes de guardia, la mayoría rotos, pero podrían ser parte de un plan. Un truco de reparar o competencia con habilidades de costura mejorará los uniformes.

No olvides también repartir los `px` del encuentro.

Desde la guarida de los grick sale otro túnel hacia la cripta.

\page

## Plan de fuga

 - Florstin está en una celda de confinamiento, con tres niveles de cansancio, 10PG y ningún espacio de conjuro. El paradero exacto solo lo pueden averiguar con un "viaje de Momo", porque está bien escondido.
 - Fiddle, Momo, o Udar podría explicar a los personajes que es posible escapar de la isla en un día de marea baja, a través de la cripta (aunque no saben cómo llegar), después subiendo el acantilado, y descendiendo rápidamente sobre un barco con cuerdas, o con un conjuro de caída de pluma para no ser vulnerables mientras bajan.
 - Uno de esos barcos podría llevar tantos presos como los jugadores consigan involucrar en su plan, pero al menos uno de los presos en el plan debe tener competencia con barcos. Si ninguno de los personajes es marinero, tendrán que involucrar a Lindra o al chivato Phil.
 - Conseguir acceder a la zona de confinamiento no debería ser fácil, pero sí viable usando conjuros adecuados y trajes de guardia obtenidos de la guarida de los grick o derrotando guardias. Llegar con Fiddle de nuevo hasta la entrada de la mina podría ser más difícil, pero el plan podría funcionar aunque les descubran en ese momento, ya que la guardia tardará en reaccionar, y los PJ podrían tapar el .
 - Si los presos tardan más de 8 horas, escucharán voces muy lejanas provenientes de la excavación. Si se acercan sigilosamente, podrán oir que los guardias comentan: "¿Habrán sido los grick? ¡Imposible, faltan demasiados, da la alarma!". Y estará justificado un combate final en el puerto. Los jugadores también pueden emboscar a los guardias que han bajado a investigar, y si no escapa ninguno, tendrán más tiempo.
 - Pero cuidado, porque si involucran a un chivato, deberían tener delante una buena pelea: calcula un encuentro _mortal_ en la tabla de umbrales siguiente, sumando el nivel de experiencia de cada jugador y PNJ, con un caballero (`700px`) y guardias (`25px`) o veteranos(`700px`). Si hay muchos presos involucrados, usa alguna forma abreviada, como por ejemplo controlar el combate de los jugadores, Florstin y Fiddle contra una cantidad razonable de enemigos, apartando al resto que "pelean en otro lado". Si los PJ prevalecen, los chivatos se unirán a la fuga si se lo ofrecen (por ej, para tener a Phil: otro marinero a bordo).
 - También puedes rescatar el combate, si te interesa, como un combate naval de un navío que sale tras ellos. Si quieres dejar abierta esa posibilidad, indica al llegar al puerto que hay más de un barco, y si los jugadores no inutilizan el resto, serán perseguidos por otro barco lleno de remeros...
 - Los aventureros pueden pedir a Fiddle/Leonora que lance conjuros para ayudarles, y se mostrará dispuesta, salvo porque querrá guardar polvo de diamante y un espacio de nivel 5 para lanzar _restablecimiento mayor_ a Florstin y otro espacio para que recupere al menos la mitad de sus PG.
 - Cuando Leonora y Florstin se encuentren, pelearán codo con codo con una coordinación perfecta, en el mismo turno de iniciativa, con un escudo a cada lado y mazas en el centro (Leonora es zurda). Leonora, además, usará _arma espiritual_.

##### Puzzle: Marea baja

El libro de las mareas, en la biblioteca, tiene la información necesaria para calcular el próximo día de marea baja.

{{note
_El Libro de las Mareas_

* La marea semanal tiene un ciclo de 7 días: cada 7 días es marea semanal baja.
* La marea decanal tiene un ciclo de 10 días: cada 10 días es marea decanal baja.
* La marea está en su punto más bajo cuando coinciden la marea baja semanal y decanal.
* Sólo en el punto de marea más baja, una vez cada 70 días, circula viento salado por las minas y se oye el batir de las olas: se abre un túnel en la isla que permitirá llegar al embarcadero.
* Cada 10 millas este la marea semanal se desfasa 1 día: la marea semanal baja es un día más tarde.
* Cada 15 millas norte la marea decanal se desfasa 1 día: la marea decanal baja es un día más tarde.

Además, uno de vosotros recuerda que la marea estaba en su punto más bajo en el puerto de Ostok hace 20 días.
Justo ese día pasaste por el puerto y alguien lo comentó.

Fiddle añade:

* Estamos a 30 millas norte y 10 millas este, de la capital.
}}

Pistas:
 - Haz una línea temporal
 - Haz una línea temporal de 70 días, comenzando por la marea baja en Ostok.
 - Marca cada marea baja semanal y decanal.

**Solución**:  (`90xp`) La marea baja será pasado mañana.

\page

## La Cripta

{{note
Casi habíais olvidado el nombre de la isla, casi os habíais acostumbrado al perenne mal olor cuando, al descender por el túnel de los grick, sentís que os acercáis al corazón de la isla, a la fuente del olor que ahora comprendéis que sólo puede venir de cadáveres en descomposición.
}}

Una búsqueda en la biblioteca Investigación CD 13 revelará 'Los secretos de la cripta', que explica todo lo necesario para resolver los puzzles de la cripta. Algunos puzzles se explican solos, pero el libro es imprescindible. Si fallan varias veces puedes por supuesto hacer que acierten automáticamente para poder avanzar.
Si no tienen el libro, se lo puede sacar Leonora o Udar de la manga, si están presentes...

##### Puzzle: la cruz de Tanis

En la habitación de abajo a la izquierda hay un pedestal con un símbolo de una cruz con un octógono. Los jugadores deben formar un octógono de 12 pies de lado. Es fácil hacerlo con una cuerda, pero deben describirlo de forma convincente y el octógono debe ser regular. Por ejemplo, pueden tomar perpendiculares, hacer bisectrices, o apoyarse en las paredes...



{{note
![cruz octógono](https://framagit.org/pang/escape_dd5e/-/raw/main/im%C3%A1genes/crux.png) {bottom:35px,margin-right:8px,width:30px;float:left}

La cruz de Tanis encierra un secreto. Para revelarlo, es necesario rodearla con sus ocho lágrimas verdes, formando un octógono perfecto.
{margin-top:7px}
}}

__Solución__:  (`60xp`) _"Al formar el octógono, se abre un compartimento secreto en la base de la cruz, revelando un collar grueso pero ligero, con piedras incrustadas, con el nombre de Leireth."_


##### Puzzle: la cripta de los muertos en vida

Esta cripta encierra zombies, vampiros y fantasmas. Si los jugadores resuelven el puzzle, sólo deberán enfrentarse a zombies.

{{note
Tenéis delante el túmulo funerario con 25 panteones. Algunos de ellos encierran la tumba de una persona o una familia importante.
La cripta está maldita, y ninguno de sus residentes está realmente muerto.
Algunos son zombies, otros fantasmas, otros vampiros.
Los panteones desocupados contienen espejos. 
Cada puerta tiene un agujero con una piedra engarzada que deja pasar una luz espectral.
La luz rebota en los espejos, y brilla con una intensidad que revela la cantidad y el tipo de panteones que el rayo de luz ha atravesado:
 - La intensidad de un rayo de luz que atraviesa una cripta de zombies aumenta.
 - La intensidad de un rayo de luz que atraviesa una cripta de vampiros aumenta, pero el reflejo de una cripta de vampiros en un espejo no hace que la intensidad aumente.
 - La intensidad de un rayo de luz que atraviesa una cripta de fantasmas no aumenta, pero el reflejo de una cripta de fantasmas sí hace que la intensidad aumente. 
}}

 - Pista: en la cripta hay 3 panteones de fantasmas, 8 de vampiros y 4 de zombies.
 - Pista: puedes permitirles que entren en un panteón y luego huyan. Los monstruos tardan algo en levantarse, pero si vuelven a pasar por el mismo panteón, los enemigos les estarán esperando.


{{monster,frame
> ___
> ## Zombi
> No muerto Mediano, neutral maligno
> - **Clase de Armadura** 8
> - **Puntos de golpe** 22 (3d8 + 9)
> - **Velocidad** 20 ft.
>___
>|FUE|DES|CON|INT|SAB|CAR
>|:---:|:---:|:---:|:---:|:---:|:---:|
>|13 (+1)|6 (-2)|16 (+3)|3 (-4)|6 (-2)|5 (-3)|
> ___
> - **Tiradas de salvación** Sab +0
> - **Inmunidades al daño** veneno
> - **Inmunidades a estados:** _Envenenado_
> ___
> - **Sentidos** Visión en la oscuridad 60 ft, Percepción pasiva 8
> - **Idiomas** Entiende los idiomas que conocía en vida, pero no puede hablar
> - **Desafío** 1/4 (50 px)
> ___
> ***Fortaleza de no muerto*** : Si el daño hace que los puntos de golpe del zombi se reduzcan a 0, este debe realizar una tirada de salvación de Constitución CD 5 más el daño recibido, a menos que el daño sea radiante o de un golpe crítico. Si tiene éxito, los puntos de golpe del zombi se reducen a 1.
> 
> ### Acciones
> ***Golpetazo*** : *Ataque de arma cuerpo a cuerpo*: +3 al ataque, alcance 5 pies, un objetivo. *Impacto*: 4 (1d6+1) puntos de daño perforante.
> 
}}

En cada panteón hay una pelea. Una familia de zombis debería ser un encuentro fácil (pero hazlo gráfico y será divertido ;-))

![zombie bourbon](https://framagit.org/pang/escape_dd5e/-/raw/main/im%C3%A1genes/zombie_bourbon.png) {width:150px;float:left;margin-right:20px}

_Idea_: Dales nombres divertidos a las familias de las criptas (políticos, reyes, famosos...), y haz que los zombies se levanten y hablen a los jugadores.
También puedes poner un caballo zombie o un oso zombie si el personaje tiene alguna anécdota con ese animal.

Elige tesoros adecuados para los personajes y/o relacionados con la familia de monstruos. 
Puede haber objetos mágicos que les ayuden durante el resto de la aventura.
En el panteón central, colgado del espejo, hay un collar con el nombre de Liridey. (`90xp` y los `px` del combate)

\page


![solución panteón](https://framagit.org/pang/escape_dd5e/-/raw/main/puzzles/panteon_sol.png) {width:200px,margin-left:30px}


##### Puzzle: la columnata

{{note
El libro es decepcionamente escueto al llegar a este enigma. Simplemente dice que :

{{dialog
Habrás de recorrer la sala de columnas en cierto orden secreto.
Falla, y se abrirán portales a otro mundos que deben permanecer cerrados.
Acierta, y se abrirá tu camino.
}}
}}

Deben pasar entre las columnas siguiendo el camino de la solución.
 - Algunas columnas están marcadas con un círculo vacío: no se debe pasar por ninguna de las cuatro aristas adyacentes.
 - Algunas columnas están marcadas con un círculo lleno a un cuarto: se debe pasar por exactamente una de las cuatro aristas adyacentes.
 - Idem para círculos a dos cuartos: se debe pasar por exactamente dos de las cuatro aristas adyacentes
 - Idem para círculos a tres cuartos: se debe pasar por exactamente tres de las cuatro aristas adyacentes

Deja que los jugadores deambulen, por desesperación. Cuando pasen cerca de una columna o entre dos columnas, se escuchará un zumbido, y la columna o columnas adyacentes brillarán y vibrarán. Si pasan dos veces, o tres, brillarán y vibrarán más fuerte.
Si una columna vibra en la medida que indica su círculo, vibrará "de forma más armoniosa".
Si cualquier columna vibra más de lo que indica su círculo, se abrirá un portal en lo alto de la columna "ofendida" y aparecerán diablos. Ídem si cierran un circuito que no pase por las columnas adecuadas.


 - Primer fallo: Un diablillo.
 - Tantos diablillos como jugadores (no cuentes PNJs).
 - Uno o más diablos espinosos o barbados (encuentro difícil o mortal). Si los jugadores piden ayuda a los otros presos, y les han armado, pon más diablos y déjales pelear en un combate aparte, que está a punto de terminar cuando los jugadores terminan el suyo ;-)


![solución panteón](https://framagit.org/pang/escape_dd5e/-/raw/main/puzzles/columnata_sol.png) {width:200px}

Si resuelven el puzzle  (`90xp` y los `px` del combate), dirígete a quien haya completado el recorrido: _"se abre un portal delante tuya en el suelo, un pequeño círculo que se levanta un palmo del suelo y desaparece, dejando a la vista un collar similar a los anteriores, pero con el nombre de Lauriat"_.

##### Puzzle: Las tres hermanas

Una entrada tallada en piedra, con tres estatuas a la entrada.

* La de la derecha tiene tallado debajo el texto: "En el centro, Leireth"
* La del centro: "soy Liridey"
* La de la izquierda: "En el centro, Lauriat"

Si los jugadores sobrepasan las estatuas, por ejemplo para inspeccionarlas, sale una banshee de cada estatua. Dale mucho drama para dejar claro que deben (y pueden) huir. Dies Irae, del requiem de Mozart, es perfecto.
Una de las hermanas podría lanzar _"Visión horripilante"_, otra _"Toque corruptor"_, y la última el _"Lamento"_, que obligaría a los compañeros a lanzarles _palabra de curación_ o similar. No perseguirán a los personajes si doblan la esquina. Si algún personaje cae y sus compañeros no le levantan, el tándem Leonora/Florstin les puede rescatar...

Si sacan tirada de historia CD9, sabrán que son hijas de un rey elfo. Supera CD15 y se les sugiere buscar en la biblioteca.
Si buscan en la biblioteca => Investigación, y tardan (14 - tirada) días en encontrar el libro de las tres hermanas:
* Lauriat siempre dice la verdad
* Leireth siempre miente
* Liridey a veces miente y a veces dice la verdad

Si ya es el último día y falta tiempo, coloca esta información en el libro de "Los Secretos de la Cripta".

El objetivo es colocar a cada hermana su collar. Deben hacerlo con mano de mago, o con una lanza, o con mucho cuidado ;-) Si intentan cruzar con los collares mal puestos, es Dies Irae de nuevo.

__Solución__: (`90xp` y los `px` del combate) De izquierda a derecha son Liridey, Leireth y Lauriat.
Lauriat no puede ser la del medio, porque nunca diría una
mentira. Así que también la primera miente. La sentada a
la derecha es Lauriat, por tanto la del medio es Leireth y la de
la izquierda Liridey.

\page

## Mapa __A__: La prisión

![La prisión](https://framagit.org/pang/escape_dd5e/-/raw/main/mapas/prison.png) {bottom:35px,margin-left:-20px,width:640px}


\page

## Mapa __B__: La cripta
{text-align:center}

![La cripta](https://framagit.org/pang/escape_dd5e/-/raw/main/mapas/lair3.png) {bottom:35px,margin-left:-10px,width:640px}


\page

## Autoría

 - Texto y puzzles: pang, kang, hang
 - Dibujos: kang y pang
 - Ficha de Oda Timbers: Jano

### Agradecimientos

Fue Clara Arévalo quien nos enseñó el potencial de los puzzles de Simon Tatham para incorporar puzzles a una historia tipo escape room. El acertijo de las tres hermanas elfas está sacado de su libro de problemas.

Todas las personas que han jugado la aventura han ayudado a mejorarla. ¡Gracias! María, Camila, Sergio, Enrique, Lucas, Carlos, Ernesto, Jano, Jorge, Eloy, Vicente, Cristina.

### Reglas

Aventura creada para el juego de rol [Dungeons&Dragons](https://dnd.wizards.com) de Wizards Of The Coast.

Esta aventura sólo necesita el sistema de reglas básico,
licenciado bajo licencia [CC-BY-4.0](https://www.dndbeyond.com/attachments/39j2li89/SRD5.1-CCBY4.0License.pdf).

### Código fuente

Todo el código fuente y material opcional

 - https://framagit.org/pang/escape_dd5e

### Licencia

Todo el contenido original se distribuye con licencias:

 - [`GFDL`](http://www.gnu.org/copyleft/fdl.html)
 - [`Creative Commons Attribution-Share Alike 4.0 License`](http://creativecommons.org/licenses/by/4.0/deed.es_ES)
 - [`cc-by-nc`](http://creativecommons.org/licenses/by-nc/4.0/deed.es_ES)

a elección del lector.

El autor entiende que aquellas imágenes que han sido generadas por https://www.craiyon.com/, también se pueden usar libremente, pero _!que me aspen si entiendo sus términos de uso :-O!_
Parece que [un juez federal ha dictaminado que las imágenes generadas por IA no están sujetas a copyright](https://www.reuters.com/legal/ai-generated-art-cannot-receive-copyrights-us-court-says-2023-08-21/).

Si quieres desarrollar más el concepto o construir sobre esta base de cualquier forma, y venderlo bajo copyright, tendrás que ponerte en contacto con el autor.

De todas formas estamos realizando ilustraciones para reemplazar esas imágenes. Puedes encontrar algunas de las ilustraciones en el fichero [imprimir/bn_dibujos.pdf](https://framagit.org/pang/escape_dd5e/-/blob/main/imprimir/bn_dibujos.pdf?ref_type=heads)

## Créditos / Herramientas

__*Maquetación*__: https://homebrewery.naturalcrit.com/

### Puzzles

 - Panteón y sala de las columnas: https://www.chiark.greenend.org.uk/~sgtatham/puzzles/
 - Estereograma: https://sites.google.com/site/gfcaprojects/openstereogram
 - Anamorfosis
   + https://es.wikipedia.org/wiki/Anamorfosis
   + https://gimp.org
 - Las tres hermanas elfas: https://framagit.org/pang/libro-de-problemas
 - La fiesta de los gigantes: https://www.concursoprimavera.es
 - Ajedrez de gigantes: https://en.wikipedia.org/wiki/Raymond_Smullyan

### Mapas

 - https://www.mapeditor.org/
 - https://wiki.wesnoth.org/BuildingMaps

### Imágenes

 - https://gimp.org/
 - https://www.craiyon.com/
 - https://openclipart.org/image/2000px/214356
 - https://openclipart.org/image/2000px/194110
 - https://www.gnu.org/software/xboard/

### Setas

 - https://es.wikipedia.org/wiki/Psilocybe_pelliculosa
 - https://es.wikipedia.org/wiki/cantharellus_cibarius
 - https://es.wikipedia.org/wiki/Amanita_muscaria
 - https://es.wikipedia.org/wiki/Amanita_phalloides
 - https://es.wikipedia.org/wiki/Agaricus_xanthodermus
 - https://es.wikipedia.org/wiki/Agaricus
 - https://commons.wikimedia.org/wiki/File:Amanita_Cesarea_%28diagrama%29.png


\page
## Apéndice I

Recorta los textos siguientes para poder repartirlos a un jugador sin que los demás lo sepan.

### Introducción

Estos textos aportan a cada personaje algo de información que los demás no saben, y que puede compartir con el resto. Se entregan al principio de la aventura, camino de la audiencia con la joven reina.

La idea es hacerles participar desde el principio, y dar al DJ un momento para rellenar su vaso.
Anímales a que lo cuenten con sus palabras, en vez de leerlo, pero también está bien si leen. Déjales que debatan un rato después, pero no mucho, porque no es positivo que se pongan demasiado conspiranoicos tan pronto.

{{recorte
Muchos valientes guerreros de tu región (las _Llanuras del Fulgor Violeta_) murieron en la guerra contra los orcos. Ahora no hay líderes claros y los ancianos temen que las regiones vecinas se aprovechen de la situación.
}}

{{recorte
El cuñado de tu tío, que es una persona importante en tu región (los _Alpes Negros_) dice que la reina es una jovenzuela sin experiencia y que allí nadie piensa que esté capacitada.
}}

{{recorte
En la corte hay gente de todos los reinos, intentando sacar provecho intrigando.
Varios cortesanos son bien conocidos en tu región (_Minas Frías_). Caciques de toda la vida que andan siempre liando para ganar más dinero.
}}

{{recorte
El marido de tu prima dice que la reina no debería fiarse de alguna gente de la corte.
}}


{{recorte
En los bares de la capital dicen que la reina es demasiado joven para gobernar, y en realidad quien gobierna es Florstin, un enano que también aconsejaba al padre de Alejandra y a su abuelo. A los nobles humanos no les gusta ser gobernados por un enano.
}}

{{recorte
Tu madre conoció a Alejandra cuando tenía 13 años, y le causó muy buena impresión. Siempre decía que quería ir a la guerra, pero su padre no le dejaba.
}}

{{recorte
El duque Florstin ha dedicado mucho tiempo a organizar las Olimpiadas. Luchó en una batalla con tu abuelo. Es buen tipo.
}}

\column

### Compañeros de remo

Estos textos se entregan durante el viaje en barco a la isla, solo a aquellos personajes que consigan hacer migas con su compañer de remo. Después pueden compartir la información con el resto.

{{recorte
__Sofi__: Sólo pudieron demostrar que tenía una deuda con la posada. Antes nunca hubieran enviado a alguien a la Isla por algo así. Se rumorea en los bajos fondos que la Isla oculta algún secreto.
}}

{{recorte
__Url__: Me detuvieron por no pagar la multa tras una pelea en un bar. Dicen que si en la prisión trabajas duro en las minas y no haces preguntas no te tratan mal, y puedes pagar tus deudas y volver.
}}

{{recorte
__Yaveis__: Me declararon culpable de sedición por oponerme a la nobleza de Alpes Negros, que envió a los pobres a morir al frente y dejó a las mujeres al cargo de las haciendas mientras subían sus precios.
Esta prisión se usaba sobre todo para presos políticos como yo, personas a las que los caciques no pueden eliminar sin un juicio público que no les interesa. Cualquiera que se oponga al pensamiento único del reino. Ahora sin embargo hay presos de todo tipo.
}}

{{recorte
__Sunda__: Me detuvieron por repartir comida que los nobles acaparaban y los veteranos necesitaban, porque sus haciendas estuvieron desatendidas mucho tiempo.
No le digas a nadie que puedo lanzar algunos conjuros, porque me han dicho que en la isla tratan duramente a los lanzadores de conjuros.
}}

{{recorte
__Yak__: Cuando volví de las guerras mi novia se había casado con un noble cretino. Le dijeron que yo había muerto. Huí con ella y me enviaron aquí.
Si no puedes ayudarme a salir de aquí mantén tu boca cerrada.
}}

\page

\page

## Apéndice II

### Setas en las cuevas

#### Amanita Buscalio (venenosa)

 - Himenio con láminas
 - Sombrero aplanado
 - Láminas libres
 - Pie con anillo y volva

#### Cantharellus Sicarius (comestible)

 - Sombrero de embudo
 - Himenio con láminas
 - Pie grueso y macizo
 - Superficie lisa o con el centro escamoso

#### Agaricus Astrodermus (tóxica)

 - Himenio con láminas
 - Sombrero aplanado
 - Láminas libres
 - Pie con anillo

#### Psilocybe pelliculera (psicotrópica)
 
 - Himenio con láminas
 - Sombrero cónico
 - Pie desnudo

\column

### Partes de la seta

![partes de la seta](https://framagit.org/pang/escape_dd5e/-/raw/main/stereo/diagrama_partes_seta.png) {width:270px}

\page

# Luis Cortés, III duque de Albión

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 

__12-2-17__ :: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 



![giraffe_ana](https://framagit.org/pang/escape_dd5e/-/raw/main/puzzles/giraffe_ana.png) {width:240px;margin-left:50px}


__18-2-17__ :: Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 


__19-2-17__ :: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 

![throne_ana](https://framagit.org/pang/escape_dd5e/-/raw/main/puzzles/throne_ana.png) {width:35px;float:right;margin-left:15px;}


__20-2-17__ :: Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 

__22-2-17__ :: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 


\column


__24-2-17__ :: Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 


__25-2-17__ :: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.


__28-2-17__ :: Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.

![Ilustracion](https://framagit.org/pang/escape_dd5e/-/raw/main/puzzles/anamorfosis_crema_2.png) {bottom:35px,margin-left:15px,width:260px}


__1-3-17__ :: Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.



\page

Esta página quizá debas imprimirla aparte, en cartulina y a color. Usa para ello el archivo de la carpeta `imprimir`.
{margin-bottom:50px;}


![puzzle Momo](https://framagit.org/pang/escape_dd5e/-/raw/main/stereo/Setas.png) {width:700px;margin-left:-35px;}



