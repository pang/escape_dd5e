# Escape_DD5e

Un aventura para el sistema __D&D 5e__ que incluye varios puzzles originales de tipo
__Escape Room__ en momentos puntuales.

## Imprimir y preparar

Te recomendamos imprimir cada parte de distinta manera. Todos los archivos para imprimir
están en la carpeta `imprimir`.

 - `bn_doble_cara_fuga.pdf`: blanco negro, doble cara
    +  El apéndice I es para recortar y repartir fragmentos entre los jugadores en el
      momento apropiado.
 - `color_mapas.pdf`: color, una sola cara
 - `bn_dibujos.pdf`: blanco y negro, una cara
    + Recorta cada personaje y muéstralo cuando entre en acción.
 - `color_cartulina_puzzle_setas.pdf`: color, en cartulina blanca

...y si vas a usar los personajes pre-diseñados:

 - `bn_doble_cara_personajes.pdf`: blanco negro, doble cara

Necesitarás dados, lapiceros y papel, pero no fichas ni tablero.

## Detalles

Más detalles en [`escape.md`](escape.md), o los pdf compilados.
La página de _Autoría_ contiene información de licencia y las herramientas que necesitas
si quieres adaptar la aventura a tus necesidades.
