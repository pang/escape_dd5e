# Retorno a la Isla Putrefacta
{text-align:center}

Continuación de _Fuga de la Isla Putrefacta_, híbrido entre __Aventura D&D 5e__ y __Escape Room__, donde los aventureros deben regresar a la isla persiguiendo al duque de Albión.
Publicada con licencia libre, al igual que la primera parte:

 - ["Fuga de la Isla Putrefacta" en Homebrewery](https://homebrewery.naturalcrit.com/share/CJ0-y-DjnSpi)
 - [Código fuente de "Fuga de la Isla Putrefacta"](https://framagit.org/pang/escape_dd5e)
 - Estamos trabajando en la tercera y última parte: _El Secreto de la Isla Putrefacta_.


{{centered
_**De 3 a 6 aventureros**_, en principio los mismos de la primera parte (nivel 3 más o menos).
}}

{{note
Los aventureros deben capturar al duque de Albión para llevarlo ante la justicia, lo que les hará volver a la Isla Putrefacta.
}}

## Comenzar

Si no hiciste la primera parte, o no tienes los personajes a mano, o alguno no sobrevivió, puedes usar los personajes prediseñados, que son los mismos de la primera parte, pero de nivel 3.

\column

### Puzzles / Escape

Pon un cronómetro en marcha cada vez que plantees un puzzle, para el crono cuando lo resuelvan o se rindan.
Dales pistas cuando se atasquen, pero anota el número de pistas, que junto con el tiempo empleado constituirá la evaluación final del escape (una evaluación informal).
Al terminar, cada personaje otorga tres tokens a quienes piensa que han contribuído más a resolver el puzzle.
Cada token vale una cantidad de `px` indicada en el puzzle.
Reparte también `px` por combates y por hitos, como entablar relación con otros presos, encontrar la guardia Grick o la biblioteca... 

Los `px` de combate se pueden repatir también de forma heterogénea, a tu elección, si algún PJ participa de forma más decisiva que otros.



\page

## Llegada a palacio

Los aventureros llegan al puerto en el barco capturado con Florstin y Leonora, y se dirigen a palacio lo más rápidamente posible.
Al llegar a palacio, haz que los aventureros cuenten lo que recuerden de la primera parte, para refrescar la memoria y asegurarte de que recuerdan las partes más importantes de la aventura anterior.

{{note
El joven Xander escucha atentamente vuestras peripecias, y con gesto severo, os cuenta:

{{dialog
La traición del duque no es ninguna sorpresa, por supuesto.
Hace un par de días que abandonó el palacio, creemos que en dirección a su fortaleza.
No pude impedírselo al no tener pruebas concluyentes.
Probablemente fue informado por Pizarro mediante una serpiente voladora...

Lo peor es que descubrimos que se ha llevado libros, dinero y objetos valiosos de palacio, incluyendo algunos objetos mágicos.

Debéis partir hacia su fortaleza, descubrir qué trama y sacarle de allá con vida. Os daré dos días de ventaja antes de enviar a mis tropas a tomar el castillo. No quiero que escape.
}}
}}

(el duque ha pagado caro a espías para robar estos objetos, que deben ser parte de su plan. Los aventureros deben tomar buena nota, para gastar el dinero que les asignarán con cabeza: si el duque robó una capa para respirar bajo el agua, necesitarán una poción de respirar bajo el agua, o algo similar `:-/`. Pero será más adelante, cuando tengan que planificar su incursión a la Isla Putrefacta...)


Xander emite orden de busca y captura contra el duque, y encarga a los aventureros que se inflitren en su fortaleza para averiguar qué está tramando.

Los aventureros reciben el equipo que tenían: el equipo inicial de su clase, y 800po cada uno, pero deben salir rápidamente para intentar pillarle por sorpresa.

### La fortaleza del Duque de Albión

{{note
La fortaleza es una isla rodeada de murallas unida al continente por un puente de tierra que queda oculto en la marea alta.
La isla-fortaleza está cerca del puerto comercial de XXX, con un barrio portuario lleno de actividad.
}}

Hay un pasadizo secreto que une la fortaleza con el puerto, con salida al almacén de una tienda de puerto.

En la taberna del puerto pueden encontrar algunos trabajadores, ex-trabajadores, y gente que conoce el castillo.
Con tiradas de persuasión, pueden conseguir información sobre el castillo.
Descubrirán rápidamente que en la planta más alta hay cuatro habitaciones: una es la del Duque, y las otras corresponden al Capitán de la Guardia, el Mago de la Corte y la Suma Sacerdotisa. Cada uno de ellos tiene una mascota distinta, es experto en un arma distinta y aficcionado a un juego distinto.

Tanto si entran desde el exterior, escalando, como si entran desde dentro, infiltrándose, deben averiguar cuál es la habitación del Duque.
Invitando a cerveza a los parroquianos pueden averiguar pistas sueltas.
Pero deben hacer tiradas de persuasión CD12, y si fallan por más de 10, o hacen pifia, o meten la pata al rolearlo, levantarán suspicacias, lo que dará lugar a que se aumente la guardia en la habitación con un caballero. Si ésto ocurre, pon además en la habitación a la misma persona con la que hablaron cuando entren en la habitación del Duque.
Con tiradas exitosas de Perspicacia CD10 antes de intentar la persuasión, podrán filtrar a los más sagaces.

Éstos son algunos de los personajes con los que podrían hablar para averiguar algo en la taberna:

 - Un camarero que lleva la comida a la planta. Muy borracho.
 - Una carpintera que montó los muebles de una de las habitaciones. Describirá los muebles con mucho detalle.
 - Un músico que solía tocar en una de las habitaciones. Dará varias pistas si cantan con él.
 - El cuidador de las mascotas, muy suspicaz si preguntan por cualquier cosa que no sea sobre las mascotas, pero dará todas las pistas relacionadas con las mascotas a un personaje que le hable de animales y que tenga competencia en trato con animales.

y éstas son las pistas que podrían obtener:

 - El experto con puñales vive en la segunda habitación.
 - El aficcionado a la apuesta de los tres dragones vive exactamente a la izquierda de la habitación del Mago.
 - El experto con bastones vive exactamente a la izquierda de la persona que juega a los dados.
 - En la segunda habitación hay un perro.
 - El experto con bastones vive exactamente a la derecha de la habitación del perro.
 - El aficcionado al ajedrez vive junto a la habitación en la que está la tarántula.
 - La habitación con serpientes voladoras está junto a la del hombre que juega a los dados.
 - El experto en martillos vive exactamente a la derecha del aficcionado a la apuesta de los tres dragones.
 - La habitación del Duque de Albión está en algún lugar entre las del experto en ballestas y la del Capitán de la Guardia, en ese orden.

Es un puzzle de tipo _Einstein_.

Pistas:
 - Es fácil sacar la solución haciendo una tabla con los posibles valores de cada columna.

__Solución:__ (`100px/token`) el Duque está en la segunda habitación.

\page


Cuando entran en la habitación se encuentran al Duque acompañado de  2x guardias y 1x noble por cada jugador. El Duque intentará estar rodeado de guardias y no atacará a los jugadores, sino que usará la acción de Esquivar mientra grita órdenes y da la voz de alarma. En cuanto los jugadores hagan daño al duque este pedirá que dejen de atacarle, y ordenará a los guardias que bajen las armas.
Si impactan a alguno de los nobles, puedes decidir que el combate pare y se rindan. Si quieren dejarlo al azar, lanza un dado por cada noble amenazado para decidir si era una persona importante para el Duque.

En el segundo o tercer turno de combate, empezarán a aparecer por la puerta guardias y/o caballeros, que los jugadores deben contener si no quieren ser sobrepasados. Los jugadores no tienen ninguna posibilidad de derrotar a todos los enemigos, su única escapatoria es capturar al duque o algún noble importante y forzar una negociación.

Si los personajes fallan por completo, pueden salir unos días más tarde como parte de un intercambio de prisioneros con Xander, pero habiendo perdido todas sus posesiones, con la mitad de sus PG, dos niveles de cansancio y sin espacios de conjuro.

Si consiguen salir de la fortaleza con el Duque, el Duque les increpa, preguntando quiénes son y exigiendo su liberación.
Si le intimidan, contará rápidamente que... TODO

También pueden interrogar al Duque al llegar al castillo. Descubren que es un noble son grandes talentos que delega en Pizarro la ejecución, y casi que la planificación, de todos sus esquemas.

## De vuelta a la isla

Los aventureros entregan al Duque al rey Xander, y se regalan un merecido descanso. Tienen oportunidad de ir a comprar suministros, etc.

Pero a la mañana siguiente son despertados pronto por el mayordomo que, sobresaltado pero solemne, les llama a las dependencias de Xander.
Xander les informa de que el Duque ha escapado, sin duda con ayuda de personal del castillo de Xander leales al Duque.

(...)

Eventualmente los aventureros descubren que el Duque ha zarpado en un barco rumbo a la Isla Putrefacta.
Se prepara una flota para tomar la Isla Putrefacta, pero los capitanes son claros en su diagnóstico: es imposible tomar la isla por mar, pues el único puerto está defendido por poderosas catapultas. La única manera de poder tomar la isla es que un comando de intrépidos aventureros las desactive.

Por supuesto, los aventureros son seleccionados para esta tarea porque conocen bien la isla.


{{note
Sois convocados a una reunión donde Xander os entrega ??? piezas de oro a cada uno.

{{dialog
Debéis regresar a la isla putrefacta para descubrir que trama el duque de Albión y luego deshabilitar las catapultas. Una vez hecho todo esto o en caso de emergencia podréis informar usando ésto:
}}

Saca de una bolsa dos _piedras mensajeras_. Le da una a Florstin y la otra a vosotros.

{{dialog
Activando vuestra piedra podréis mandar un mensaje a Florstin y él os lo responderá.
Florstin estará esperando a vuestra señal a una distancia prudencial y cuando le digáis comenzará el asalto a la Isla.
}}

Tanto el puerto como el acceso por la cripta por dónde escapasteis, estarán fuertemente protegidos, y no podréis entrar por allí.
Sin embargo, la bibliotecaria del complejo, Atena, nos ha contactado y ha mencionado la existencia de una segunda gruta. Según nuestros cálculos, estará completamente sumergida pero podríais acceder con esta poción de _respirar bajo el agua_ (os entrega una botella de un líquido azulado).
}}



\page
## La galería

\page

## Resolución

### La historia de la fortaleza

La fortaleza de la Casa Ñadre fue construída en la isla de X por su situación estratégica, no lejos del continente y fácil de defender, pero también porque Drusilia, la primera matrona de Ñadre descubrió un pasaje que la conectaba con el Underdark, y que quiso proteger estableciéndose allí con su familia.

Los elfos oscuros del Underdark eventualmente descubrieron el pasaje, pero durante varias generaciones no hubo conflicto porque los drow no tenían mayor interés en el mundo exterior.
Sin embargo, hace alrededor de quinientos años surgió un conflicto entre los elfos de la isla y la familia drow Facta, que aspiraban a controlar el puerto de la isla para comerciar con traficantes de esclavos.

Como parte de su plan para controlar el castillo, los drows de la familia Facta infectaron de la Plaga del Caos al líder de los elfos de la fortaleza, Lumiel Ñadre, convirtiéndole así en un slaad verde, y a unos pocos elfos más, que se dedicaron a propagar la Plaga intentando no ser descubiertos. El líder se mantuvo oculto entre los elfos hasta convertirse en slaad de la muerte y entonces se descubrió la Plaga.

Los slaads se propagaron también por el Underdark, y el resto de los drow rápidamente identificaron a la familia Facta como responsable. Elfos y drows unieron fuerzas contra los slaad y la familia Facta.
Sólo una drow de la familia Facta sobrevivió al ataque, la sacerdotisa del Caos Putria, que para escapar se dejó infectar por un slaad, pero consiguió mantener el control del slaad tras la infección incrustándole un fragmento de la _piedra primigenia_ (la piedra que dio lugar a los slaads) al renacuajo slaad según salía de su vientre.

Los slaads, liderados por el slaad de la muerte que fue Lumiel Ñadre, se encontraban en enfrentados en una cruenta batalla contra los elfos de la fortaleza y los drow, liderados por B´lock Ckuver.
B´lock Ckuver se estaba enfrentando él solo a Lumiel, y cuando la batalla parecía perdida apareció Putria Facta y derrotó a Lumiel, le pidió a Ckuver que le perdonara, con la esperanza de recuperar el favor de  Lolth y las otras casas drow. 

B´lock Ckuver no le hizo caso e intentó matarla. Putria se enfadó tanto que perdió el control del slaad y le empezó a matar. Cerca del final de la batalla aparecieron unos drows

B´lock Ckuver

Roth Zurll

 Sadouth



Sin embargo, ni los elfos ni los drow la perdonaron, y Putria 

Cuando la casa Ñadre y los drow estaban

La casa Ñadre y 

Los elfos  provocaron un terremoto

Atena es un slaad y está ayudando a los aventureros para...


\page

## Créditos / Herramientas

__*Maquetación*__: https://homebrewery.naturalcrit.com/

### Puzzles

 - Puzzle de Einstein: https://www.ahapuzzles.com/logic/zebra/basic-3/
